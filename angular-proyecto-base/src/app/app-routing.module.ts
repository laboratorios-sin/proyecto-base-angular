import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InicioComponent } from './componentes/inicio/inicio.component';
import { PageNotFoundComponentComponent } from './componentes/page-not-found-component/page-not-found-component.component';
import { ToatsSweetComponent } from './componentes/toats-sweet/toats-sweet.component';

const routes: Routes = [
  {
    path:'inicio',
    component:InicioComponent
  },
  {
    path:'toast-sweet',
    component:ToatsSweetComponent
  },
  { path: '',   redirectTo: '/inicio', pathMatch: 'full' },
  {
    path:'input-output',
    loadChildren: ()=>import('./input-ouput/input-ouput.module').then(m=>m.InputOuputModule)
  },
  {
    path:'directivas',
    loadChildren: ()=>import('./directivas/directivas.module').then(m=>m.DirectivasModule)
  },
  {
    path:'reactividad',
    loadChildren: ()=>import('./reactividad/reactividad.module').then(m=>m.ReactividadModule)
  },
  {
    path:'redux',
    loadChildren: ()=>import('./redux/redux.module').then(m=>m.ReduxModule)
  },
  {
    path : 'kanban-firebase',
    loadChildren: ()=>import('./kanban-firebase/kanban-firebase.module').then(m=>m.KanbanFirebaseModule)
  },
  { path: '**', component: PageNotFoundComponentComponent }, 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
