import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ToatsSweetComponent } from './toats-sweet.component';

describe('ToatsSweetComponent', () => {
  let component: ToatsSweetComponent;
  let fixture: ComponentFixture<ToatsSweetComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ToatsSweetComponent]
    });
    fixture = TestBed.createComponent(ToatsSweetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
