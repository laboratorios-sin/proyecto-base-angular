import { Component } from '@angular/core';
import { ToastrService } from 'ngx-toastr'; 
import Swal from 'sweetalert2';

@Component({
  selector: 'app-toats-sweet',
  templateUrl: './toats-sweet.component.html',
  styleUrls: ['./toats-sweet.component.css']
})
export class ToatsSweetComponent {

  constructor(private toastr: ToastrService) { }

  showSuccess() {
    this.toastr.success(' Se actualizo con correcto', 'OK',
      { timeOut: 2000 });
  }

  showError() {
    this.toastr.error('se se pudo actualizar', 'ERROR', {
      timeOut: 3000
    });
  }

  viewMessage(){
    Swal.fire('Any fool can use a computer')
  }



}
