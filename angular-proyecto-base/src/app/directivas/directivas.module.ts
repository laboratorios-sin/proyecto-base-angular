import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DirectivasRoutingModule } from './directivas-routing.module';
import { PrincipalComponent } from './principal/principal.component';
import { ResaltadorAmarilloDirective } from './resaltador-amarillo.directive';


@NgModule({
  declarations: [
    PrincipalComponent,
    ResaltadorAmarilloDirective
  ],
  imports: [
    CommonModule,
    DirectivasRoutingModule
  ]
})
export class DirectivasModule { }
