import { Component } from '@angular/core';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent {

  tab_active = 0;

  cursos: Curso[];

  constructor() { 
    this.cursos = [
      {nombre:"Angular",nivel:"Basico",descripcion:'Framework'},
      {nombre:"VueJs",nivel:"Basico",descripcion:'Library'},
      {nombre:"ReadJs",nivel:"Intermedio",descripcion:'Framework'},
      {nombre:"Flutter",nivel:"Basico",descripcion:'Framewrok'},
      {nombre:"Python",nivel:"Avanzado",descripcion:'Lenguaje'},
    ];
  }


}

interface Curso{
  nombre : string;
  nivel : string;
  descripcion : string;
}

