import { Directive,ElementRef,HostListener  } from '@angular/core';

@Directive({
  selector: '[appResaltadorAmarillo]'
})
export class ResaltadorAmarilloDirective {

  constructor(private el: ElementRef) { }

  //constructor(private el: ElementRef) {
  //   this.el.nativeElement.style.backgroundColor = 'yellow';
  //}

  @HostListener('mouseenter') onMouseEnter() {
    this.resaltador('yellow');
  }
  
  @HostListener('mouseleave') onMouseLeave() {
    this.resaltador('');
  }
  
  private resaltador(color: string) {
    this.el.nativeElement.style.backgroundColor = color;
  }


}
