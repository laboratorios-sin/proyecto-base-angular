import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';

import { KanbanFirebaseRoutingModule } from './kanban-firebase-routing.module';
import { TareaComponent } from './tarea/tarea.component';
import { TareasComponent } from './tareas/tareas.component';


@NgModule({
  declarations: [
    TareaComponent,
    TareasComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    KanbanFirebaseRoutingModule
  ]
})
export class KanbanFirebaseModule { }
