import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Tarea } from '../tarea';

@Component({
  selector: 'app-tarea',
  templateUrl: './tarea.component.html',
  styleUrls: ['./tarea.component.css']
})
export class TareaComponent {

  @Input() tarea: Tarea | null = null;
  @Output() editar = new EventEmitter<Tarea>();
  @Output() eliminar = new EventEmitter<Tarea>();

  constructor() { }
  
  ngOnInit(): void {
  }

}
