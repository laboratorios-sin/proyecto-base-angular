import { Injectable } from '@angular/core';
import { Tarea } from './tarea';
import { Firestore, collection, addDoc, collectionData, deleteDoc, setDoc, doc } from '@angular/fire/firestore'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TareasService {

  constructor(private firestore: Firestore) { }

  addTarea(tar: Tarea) {
    const tarearef = collection(this.firestore, 'tareas');
    addDoc(tarearef, tar)
  }

  getTareas(): Observable<Tarea[]> {
    const tarearef = collection(this.firestore, 'tareas');
    return collectionData(tarearef, { idField: 'id' }) as Observable<Tarea[]>
  }

  removeTarea(tar: Tarea) {
    const tarearef = doc(this.firestore, `tareas/${tar.id}`);
    return deleteDoc(tarearef)
  }
  
  updateTarea(tar: Tarea) {
    const tarearef = doc(this.firestore, `tareas/${tar.id}`);
    return setDoc(tarearef, tar);
  }

}
