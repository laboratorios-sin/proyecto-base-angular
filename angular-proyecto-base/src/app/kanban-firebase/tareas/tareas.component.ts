import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Tarea } from '../tarea';
import { TareasService } from '../tareas.service';

@Component({
  selector: 'app-tareas',
  templateUrl: './tareas.component.html',
  styleUrls: ['./tareas.component.css']
})
export class TareasComponent {

  tareas: Tarea[];
  openModal: string = 'none';
  tarea: Tarea;
  tareaEdit: Tarea;
  accion: string;

  constructor(private tareasService: TareasService) {
    this.tarea = { titulo: '', descripcion: '', estado: '' };
    this.tareaEdit = { titulo: '', descripcion: '', estado: '' };
    this.accion = 'add';
    this.tareas = []
  }

  ngOnInit(): void {
    this.cargarTareas();
  }

  cargarTareas() {
    this.tareasService.getTareas().subscribe(resp => {
      this.tareas = [...resp]
    })
  }

  async registrarTarea() {
    //this.tareas.push(this.tarea);
    this.tareasService.addTarea(this.tarea);
    this.tarea = { titulo: '', descripcion: '', estado: '' };
  }

  onRegistraTarea() {
    if (this.accion == 'edit') {
      this.tareasService.updateTarea(this.tarea);
    } else {
      this.tareasService.addTarea(this.tarea);
    }
    this.openModal = 'none';
    this.tarea = { titulo: '', descripcion: '', estado: 'Backlog' };
    this.accion = 'add'
  }

  editarTarea(tar: Tarea) {
    this.tarea = { ...tar };
    this.openModal = 'block';
    this.accion = "edit";
  }
  
  eliminarTarea(tar: Tarea) {
    this.tareasService.removeTarea(tar);
  }

}
