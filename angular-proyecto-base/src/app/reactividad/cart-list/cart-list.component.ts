import { Component } from '@angular/core';
import { StoreService } from '../servicios/store.service';

@Component({
  selector: 'app-cart-list',
  templateUrl: './cart-list.component.html',
  styleUrls: ['./cart-list.component.css']
})
export class CartListComponent {

  myCart$ = this.storeService.myCart$;

  constructor(
    private storeService : StoreService
  ){}

  loadList(){
    this.storeService.myCart$;
  }

}
