import { Component } from '@angular/core';
import { StoreService} from '../servicios/store.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {

  myCart$ = this.storeService.myCart$;

  openMenuIsq : string = 'none';

  constructor(private storeService:StoreService){

  }

}
