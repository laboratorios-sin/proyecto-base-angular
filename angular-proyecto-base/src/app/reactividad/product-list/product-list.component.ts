import { Component } from '@angular/core';
import { ProductService } from '../servicios/product.service';
import { ProductList, Product } from '../modelos/product';

import {StoreService} from '../servicios/store.service';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent {

  products: Product[];
  productList: ProductList;

  constructor(
    private storeService:StoreService,
    private productService: ProductService) {
    this.products = []
    this.productList = { products: [], total: 0, skip: 0, limit: 0 }
  }

  ngOnInit(): void {
    this.cargarProductos();
  }

  cargarProductos() {

    Swal.fire({
      text:'Recuperando informacion ...'
    });
    Swal.showLoading();
    this.productService.obtenerRegistros().subscribe(resp => {
      this.productList = resp as ProductList;
      this.products = this.productList.products;
      Swal.close();
    });

  }

  addProductToCart(product:Product){
    this.storeService.addProduct(product);
  }

}
