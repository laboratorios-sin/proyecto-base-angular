import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactividadRoutingModule } from './reactividad-routing.module';
import { ProductListComponent } from './product-list/product-list.component';
import { NavbarComponent } from './navbar/navbar.component';
import { CartListComponent } from './cart-list/cart-list.component';


@NgModule({
  declarations: [
    ProductListComponent,
    NavbarComponent,
    CartListComponent
  ],
  imports: [
    CommonModule,
    ReactividadRoutingModule
  ]
})
export class ReactividadModule { }
