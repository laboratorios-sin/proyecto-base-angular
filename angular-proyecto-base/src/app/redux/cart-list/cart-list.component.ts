import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { Product } from '../modelos/product';
import { loadProducts } from '../product.actions';
import { ProductState } from '../product.reducer';

@Component({
  selector: 'app-cart-list',
  templateUrl: './cart-list.component.html',
  styleUrls: ['./cart-list.component.css']
})
export class CartListComponent implements OnInit {

  myCart$ = new Observable<Product[]>;

  constructor(private store: Store<{cart : ProductState}> ) {
    this.myCart$ = this.store.select(state=>state.cart.products);
  }

  ngOnInit(){
    //this.loadCart();
  }

  loadCart(){
    this.store.dispatch(loadProducts());
  }
  

}
