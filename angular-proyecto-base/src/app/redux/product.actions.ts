
import { createAction, props } from '@ngrx/store';
import {  Product } from './modelos/product';

export const loadProducts = createAction(
    '[Product] Load Products');

export const addProduct = createAction(
    '[Product] Add Product', 
    props<{ product: Product }>()
);

export const deleteProduct = createAction(
    '[Product] Delete Product', 
    props<{ productId: number }>()
);
