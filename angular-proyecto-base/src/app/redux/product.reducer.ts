import { createReducer, on} from '@ngrx/store';
import { addProduct,loadProducts } from './product.actions'; 
import { Product } from './modelos/product';

export interface ProductState {
  products: Product[];
}

export const initialProductState: ProductState = {
  products: [],
};

export const productReducer = createReducer(
  initialProductState,
  on(addProduct, (state, { product }) => ({
    ...state,
    products: [...state.products, product],
  })),
  on(loadProducts,state=>state)
  
);

