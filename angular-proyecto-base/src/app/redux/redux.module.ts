import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReduxRoutingModule } from './redux-routing.module';
import { CartListComponent } from './cart-list/cart-list.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ProductListComponent } from './product-list/product-list.component';

import { BrowserModule } from '@angular/platform-browser';
import { counterReducer } from './counter.reducer';
import { productReducer } from './product.reducer';

import { StoreModule } from '@ngrx/store';



@NgModule({
  declarations: [
    CartListComponent,
    NavbarComponent,
    ProductListComponent,
  ],
  imports: [
    CommonModule,
    ReduxRoutingModule,
    StoreModule.forRoot({ product: productReducer}),
  ]
})
export class ReduxModule { }
