import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Product, ProductList } from '../modelos/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  URL_API: string = 'https://dummyjson.com/products';

  constructor(private http: HttpClient) { }

  obtenerRegistros() {
    return this.http.get(this.URL_API);
  }

  nuevoRegistro(product: Product) {
    return this.http.post(this.URL_API, product);
  }

  modificarRegistro(product: Product) {
    return this.http.put(`${this.URL_API}/${product.id}`, product);
  }

  eliminarRegistro(id: Number) {
    return this.http.delete(`${this.URL_API}/${id}`);
  }

}
